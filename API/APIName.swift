//
//  APIName.swift
//  Khojees
//
//  Created by Adeesh on 09/01/17.
//  Copyright © 2017 Vivacious. All rights reserved.
//

import UIKit
import MagicalRecord
import Alamofire
import SwiftyJSON

let API_APIKEY = "5987ac798eb9c2ed5801E2E6E2pkJ8"

let API_LOGIN = "login"

let API_REGISTRATION = "sign_up"

let API_SEARCH_GAME = "search_service"

let API_MISSION_LIST = "mission_list"

let API_MISSION_COMPLETE_LIST = "completed_mission_list"

let API_Upload_Pic = "upload_pic"

let API_FEED = "feed"

let API_USER_PROFILE = "user_profile"

let API_GET_PHOTO = "get_pic"

let API_USER_HISTORY = "userHistory"

let API_RANK = "ranking"

let API_USER_DETAILS = "user_details"

let API_DELETE_MISSION = "delete_mission"

let API_UPDATE_MISSION = "update_mission"

public class API: NSObject {
    
    struct Singleton {
        static let sharedInstance = API()
    }
    
    public class var sharedInstance: API {
        return Singleton.sharedInstance
    }
    
    public typealias completionHandler = ((Bool,APIResponseHandler?)->Void)?
    
    public typealias completionHandlerString = ((Bool,String?)->Void)?
    
    public typealias completionHandlerDict = ((Bool,JSON)->Void)?
    
    
    internal func getResponseHandler(_ dictTemp:Dictionary<String,Any?>!)->APIResponseHandler{

        let obj:APIResponseHandler = APIResponseHandler(dictionary: dictTemp)
        return obj

    }
  
    private func displayError(error:NSError!,needDismiss:Bool = false){
        
        if error != nil {
            if error.code == -1009 || error.code == -1001{
                
                Functions.displayAlert(NO_NETWORK,needDismiss: needDismiss);
                
                
            }else{
                
                Functions.displayAlert(SERVER_ERROR,needDismiss: needDismiss)
                
                
            }
        }
        
    }
    
    

    
    
    func register_Devices(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                    self.displayError(error: error, needDismiss: false)
                    block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    func login(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
       
                    self.displayError(error: error, needDismiss: false)
                     block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func register_user(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                    self.displayError(error: error, needDismiss: false)
                     block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func GameSearch(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                
                
                    self.displayError(error: error, needDismiss: false)
                     block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func Mission_list(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                
                
                    self.displayError(error: error, needDismiss: false)
                     block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    func Mission_Complete_list(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
            
                    self.displayError(error: error, needDismiss: false)
                     block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func Submit_Evidence_Text(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    
    
    func Submit_Evidence_Photo(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
//    func Submit_Evidence_Photo(dictrequest:Dictionary<String,AnyObject>?,image:UIImage?,block:completionHandler) {
//        
//        
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            
//            if let _image = image {
//                if let imageData = UIImageJPEGRepresentation(_image, 0.5) {
//                    multipartFormData.append(imageData, withName: StrPHOTO, fileName:"avatar.png" , mimeType: "image/png")
//                }
//            }
//            
//            for (key, value) in dictrequest! {
//                multipartFormData.append(value.data!, withName: key)
//                
//            }
//            
//        }, to: API_URL , encodingCompletion: { (result) in
//            
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.responseJSON { response in
//                    
//                    
//                    let json = JSON(response.result.value!)
//                    
//                    let responseHandler = self.getResponseHandler(json.dictionaryObject as Dictionary<String, AnyObject>?)
//                    
//                    if  (responseHandler.error) {
//                        
//                            Functions.displayAlert(SERVER_ERROR ,needDismiss: true)
//            
//                        block?(false,responseHandler)
//                        
//                    }else{
//                        
//                        block?(true,responseHandler)
//                    }
//                    
//                }
//                
//                
//            case .failure(let encodingError):
//    
//                    self.displayError(error: encodingError as NSError!, needDismiss: false)
//                     block?(false,nil)
//                print(encodingError)
//            }
//            
//        })
//        
//    }
    
    func Feed(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func Profile(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    func Get_Photos(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func User_History(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    func Rank(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    func UserDetails(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func Delete_Mission(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func Update_Text_Mission(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func Update_Photo_Mission(dictrequest:Dictionary<String,AnyObject>?,block:completionHandler){
        
        Functions.apiPostCall(apiName: "", parameters: dictrequest) { (json, error) -> Void in
            
            if error != nil {
                
                self.displayError(error: error, needDismiss: false)
                block?(false,nil)
                
            }else{
                let responseHandler = self.getResponseHandler(json)
                
                if  (responseHandler.error) {
                    block?(false,responseHandler)
                    
                }else{
                    block?(true,responseHandler)
                    
                }
                
                
            }
            
        }
        
    }
//    func Update_Photo_Mission(dictrequest:Dictionary<String,String>?,image:UIImage?,block:completionHandler) {
//        
//        
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            
//            if let _image = image {
//                if let imageData = UIImageJPEGRepresentation(_image, 0.5) {
//                    multipartFormData.append(imageData, withName: "image", fileName: StrPHOTO, mimeType: "image/jpeg")
//                }
//            }
//            
//            for (key, value) in dictrequest! {
//                
//                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//                
//            }
//            
//        }, to: API_URL , encodingCompletion: { (result) in
//            
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.responseJSON { response in
//                    
//                    
//                    let json = JSON(response.result.value!)
//                    
//                    let responseHandler = self.getResponseHandler(json.dictionaryObject as Dictionary<String, AnyObject>?)
//                    
//                    if  (responseHandler.error) {
//                        
//                        Functions.displayAlert(SERVER_ERROR ,needDismiss: true)
//                        
//                        block?(false,responseHandler)
//                        
//                    }else{
//                        
//                        block?(true,responseHandler)
//                    }
//                    
//                }
//                
//                
//            case .failure(let encodingError):
//                
//                self.displayError(error: encodingError as NSError!, needDismiss: false)
//                block?(false,nil)
//                print(encodingError)
//            }
//            
//        })
//        
//    }
    
}
