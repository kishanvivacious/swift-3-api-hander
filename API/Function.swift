//
//  Function.swift
//  Khojees
//
//  Created by Adeesh on 09/01/17.
//  Copyright © 2017 Vivacious. All rights reserved.
//

import MagicalRecord
import netfox
import SwiftyJSON
import Alamofire

class Functions : Alamofire.SessionManager {
    
    weak var request: Alamofire.Request?
    
    
    public static let sharedManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses?.insert(NFXProtocol.self, at: 0)
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 40
        configuration.timeoutIntervalForResource = 40
        
        let manager = Alamofire.SessionManager(configuration: configuration)
        return manager
    }()
    
    override init(configuration: URLSessionConfiguration, delegate: SessionDelegate, serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        super.init(configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
    }
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open class func setPreferences(_ val: Any!, forkey key: String!) {
        let userDefault: UserDefaults = UserDefaults.standard
        userDefault.set(val, forKey: key)
        userDefault.synchronize()
    }
    
    open class func removePreferences(_ key: String!) {
        let userDefault: UserDefaults = UserDefaults.standard
        userDefault.removeObject(forKey: key)
        userDefault.synchronize()
        
    }
    
    open class func getPreferences(_ key: String!) -> AnyObject! {
        let userDefault: UserDefaults = UserDefaults.standard
        return userDefault.object(forKey: key) as AnyObject!
    }
    
    
    open class func displayAlert(_ msg: String!, needDismiss: Bool = false, title: String = APP_NAME) {
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel) {(action) in
            if needDismiss {
                UIViewController.currentViewController()!.dismiss(animated: true, completion: nil)
            }
            
        }
        alertController.addAction(defaultAction)
        
        UIViewController.currentViewController()!.present(alertController, animated: true, completion: nil)
    }
    
    class func displayAlertWithTwoButtons(message:String,title: String = APP_NAME,button1Title:String,button2Title:String,actionBlock:@escaping (_ isOkClicked:Bool)->Void) {
        
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        
        let button1TitleAction = UIAlertAction(title: button1Title, style: .default) { (action) in
            actionBlock(false)
        }
        alertController.addAction(button1TitleAction)
        
        
        let button2TitleAction = UIAlertAction(title:button2Title, style: .default) { (action) in
            
            actionBlock(true)
            
        }
        alertController.addAction(button2TitleAction)
        
        UIViewController.currentViewController()!.present(alertController, animated: true, completion: nil)
    }
    
    open class func updateCoreData(_ mainblock: @escaping (_ sucess: Bool?, _ error: NSError?) -> Void) {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore {(s, e) -> Void in
            if s {
                mainblock(s, nil)
            } else {
                mainblock(false, e as NSError?)
            }
        }
    }
    open class func updateCoreData() -> Void {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore {(s, e) -> Void in
            
        }
        
    }
    
    
    class func apiPostCall(apiName: String, parameters: Dictionary<String, AnyObject>?, block mainBlock: ((Dictionary<String, AnyObject>?, NSError?) -> Void?)?) {
        
        let requestConvertible:URLRequestConvertible
        requestConvertible = AlamofireRequest.construct(Alamofire.HTTPMethod.post, apiName, headers: self.getRequestHeaders(), perms: parameters)
        Functions.sharedManager.request(requestConvertible).debugLog().responseString { response in
            
            print(response)
            //self.validateResponseError(response)
            
            }.debugLog().responseSwiftyJSON {(request, response, json, error) -> Void in
                
                if Config.APP_API_LOGGING_ENABLE {
                    
                    debugPrint("REQUEST URL : =================================")
                    debugPrint(request.url!)
                    debugPrint("RESPONSE : =================================")
                    debugPrint(json)
                    debugPrint("=================================")
                    
                }
                
                mainBlock?(json.dictionaryObject as Dictionary<String, AnyObject>?, error)
                
        }
        
        
    }
    
    
    class func apiPutCall(apiName: String, parameters: Dictionary<String, AnyObject>?, block mainBlock: ((Dictionary<String, AnyObject>?, NSError?) -> Void?)?) {
        
        let requestConvertible:URLRequestConvertible
        requestConvertible = AlamofireRequest.construct(Alamofire.HTTPMethod.put, apiName, headers: self.getRequestHeaders(), perms: parameters)
        
        Functions.sharedManager.request(requestConvertible).debugLog().responseString { response in
            
            
            //self.validateResponseError(response)
            
            }.debugLog().responseSwiftyJSON {(request, response, json, error) -> Void in
                
                if Config.APP_API_LOGGING_ENABLE {
                    
                    debugPrint("REQUEST URL : =================================")
                    debugPrint(request.url!)
                    debugPrint("RESPONSE : =================================")
                    debugPrint(json)
                    debugPrint("=================================")
                    
                }
                
                mainBlock?(json.dictionaryObject as Dictionary<String, AnyObject>?, error)
                
        }
        
        
    }
    
    
    class func apiGetCall(apiName: String, parameters: Dictionary<String, AnyObject>? = nil, block mainBlock: ((Dictionary<String, AnyObject>?, NSError?) -> Void?)?) {
        
        
        
        self.sharedManager.request(AlamofireRequest.construct(Alamofire.HTTPMethod.get, apiName, headers: getRequestHeaders(), perms: parameters)).debugLog().responseString { response in
            //validateResponseError(response:response)
            }.responseSwiftyJSON {(request, response, json, error) -> Void in
                
                
                if Config.APP_API_LOGGING_ENABLE {
                    
                    debugPrint("REQUEST URL : =================================")
                    debugPrint(request.url!)
                    debugPrint("RESPONSE : =================================")
                    debugPrint(json)
                    debugPrint("=================================")
                    
                }
                
                mainBlock?(json.dictionaryObject as Dictionary<String, AnyObject>?, error)
          
        }
        
        
    }
    
    
    
    class func apiPutUserUpdate(apiName: String, parameters: Dictionary<String, AnyObject>? = nil, block mainBlock: ((Dictionary<String, AnyObject>?, NSError?) -> Void?)?) {
        
        
        
        self.sharedManager.request(AlamofireRequest.construct(Alamofire.HTTPMethod.put, apiName, headers: getRequestHeaders(), perms: parameters)).debugLog().responseString { response in
            //validateResponseError(response:response)
            }.responseSwiftyJSON {(request, response, json, error) -> Void in
                
                
                if Config.APP_API_LOGGING_ENABLE {
                    
                    debugPrint("REQUEST URL : =================================")
                    debugPrint(request.url!)
                    debugPrint("RESPONSE : =================================")
                    debugPrint(json)
                    debugPrint("=================================")
                    
                }
                
                mainBlock?(json.dictionaryObject as Dictionary<String, AnyObject>?, error)
             
        }
        
        
    }
    
    
    class func apiDeleteCall(apiName: String, parameters: Dictionary<String, AnyObject>? = nil, block mainBlock: ((Dictionary<String, AnyObject>?, NSError?) -> Void?)?) {
        
        
        self.sharedManager.request(AlamofireRequest.construct(Alamofire.HTTPMethod.delete, apiName, headers: getRequestHeaders(), perms: parameters)).debugLog().responseString { response in
            //validateResponseError(response)
            }.responseSwiftyJSON {(request, response, json, error) -> Void in
                
                if Config.APP_API_LOGGING_ENABLE {
                    
                    debugPrint("REQUEST URL : =================================")
                    debugPrint(request.url!)
                    debugPrint("RESPONSE : =================================")
                    debugPrint(json)
                    debugPrint("=================================")
                    
                }
                
                mainBlock?(json.dictionaryObject as Dictionary<String, AnyObject>?, error)

        }
  
    }

    private class func getRequestHeaders() -> [String: String] {
        
        return ["mobile-app": "true"]
    }
 
    
    class func Dict_to_object(dict : Dictionary<String,Any>,obj : Any) -> Any{
        for(key,value) in dict
        {
            if ((obj as AnyObject).responds(to: NSSelectorFromString(key)))
            {
                (obj as AnyObject).setValue(value, forKey: key)
            }
        }
        return obj
    }
    
      
//    
//    //MARK: Convert Dict to Obj
//    class func Dict_to_object(dict:Dictionary<String,AnyObject?>,obj:AnyObject)->AnyObject  {
//        for (key, value) in dict {
//            
//            if (obj.respondsToSelector(NSSelectorFromString(key))) {
//                obj.setValue(value, forKey: key)
//            }
//        }
//        return obj
//    }
    
    
    
}
